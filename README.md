# GolfXTRM #
*First person golf game with no HUDs, no aiming aids, no course map, no teleport between shots, and nothing to help you find the ball.*

This game was written in one week for Fuck This Jam 2012.

### Concept ###
> "Golf games seem to be about watching power meters and adjusting targeting sights. I prefer being outdoors, exploring procedural landscapes, and playing with physics simulations. So here's my take on the genre." -SlowRiot

## Info ##
* http://golfxtrm.com

## Download ##
* http://voxelstorm.itch.io/golfxtrm

## Bug reporting ##
* http://code.voxelstorm.com/golfxtrm/issues